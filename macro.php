<?php
$osInfo = strtoupper(substr(PHP_OS, 0, 3));

if (count($argv) <= 4)
{
    echo 'argv enter. area stage isChallenge device deviceip ex) 5 3 1 genymotion (101)' . PHP_EOL;
    exit;
}
$area = $argv[1];
$stage = $argv[2];
$isChallenge = intval($argv[3]);
$device = $argv[4];
$data = json_decode(file_get_contents("$device.json"), true);
if ($data == null) {
    echo 'Error!' . PHP_EOL;
    exit;
}
$deviceName = $data['deviceName'];
if ($device == 'genymotion') {
    $deviceIp = (isset($argv[5]) && $argv[5] != '') ? $argv[5] : '101';
    $deviceName = "192.168.56.{$deviceIp}:5555";
} else {
    $deviceIp = $device;
}
echo "area = $area, stage = $stage, isChallenge = $isChallenge, deviceName = $deviceName" . PHP_EOL;

// 최초 실행하기 위함.
$isFirst = true;
// 버튼이 클릭이 안될 때 재시작하기 위한 카운트.
$stopCount = 0;

$startStoryResult = false;
$startStoryResult2 = false;
$startFightResult = false;
$startTowerResult = false;
$startOrdealResult = false;
// story, fight, tower
$mode = 'story';
//$mode = 'fight';
//$mode = 'tower';
//$mode = 'ordeal';

// 서포터즈 10번 카운트 후에 호출하기 위한 조치.
$supportCount = 0;

while(true)
{
    // 최초 genymotion 실행.
    if ($isFirst)
    {
        if ($device == 'genymotion') {
            exec("adb -s {$deviceName} shell chmod 440 /proc/modules");
            exec("adb -s {$deviceName} shell chmod 440 /proc/version");
            exec("adb -s {$deviceName} push xigncode.xel /sdcard/Android/data/com.wellbia.xigncode/com.ftt.hero_gl_4kakao/xigncode.xel");
            exec("adb -s {$deviceName} push xmag.xem /sdcard/Android/data/com.wellbia.xigncode/com.ftt.hero_gl_4kakao/xmag.xem");
            exec("adb -s {$deviceName} push xtmp.xem /sdcard/Android/data/com.wellbia.xigncode/com.ftt.hero_gl_4kakao/xtmp.xem");
        }
        exec("adb -s {$deviceName} shell am start -n com.ftt.hero_gl_4kakao/com.ftt.hero_gl_4kakao.AndroidNativeBridge");
        $isFirst = false;
        $stopCount = 0;
    }
    // screen capture
    echo "screen capture..." . PHP_EOL;
    if ($osInfo == 'DAR')
    {
        system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen_{$deviceIp}.png");
    }
    else if ($osInfo == 'WIN')
    {
        system("adb -s {$deviceName} shell screencap -p /sdcard/screen_{$deviceIp}.png");
        system("adb -s {$deviceName} pull /sdcard/screen_{$deviceIp}.png");
        system("adb -s {$deviceName} shell rm /sdcard/screen_{$deviceIp}.png");
    }
    $img = imagecreatefrompng("screen_{$deviceIp}.png");
    echo 'mode = ' . $mode . PHP_EOL;

    checkKey('touchScreen');
    checkKey('closeAd');
    checkKey('noticeOk');
    if (!checkKey('sleepReward')) {
        checkKey('sleepRewardX');
        checkKey('sleepRewardX2');
    }
    checkKey('honorOK');
    checkKey('helpFriend');
    checkKey('retry');
    checkKey('networkRetry');
    checkKey('networkRetry2');
    checkKey('networkRetry3');

    // 스토리 모드
    if ($mode == 'story') {
        if (checkKey('storyMode')) {
            // 파티관리 대표로 변경.
            sleep(3);
            clickKey('storyMode1');
            sleep(3);
            clickKey('storyMode2');
            sleep(2);
            clickKey('storyMode3');
        }

        // 스테이지 선택화면인지 체크.
        if (checkPixelKey('storyCheckStage')) {
            // 현재 스테이지 찾기.
            $currentArea = 1;
            if (checkPixelKey('storyCheckArea1')) {
                $currentArea = 1;
            }
            else if (checkPixelKey('storyCheckArea2')) {
                $currentArea = 2;
            }
            else if (checkPixelKey('storyCheckArea3')) {
                $currentArea = 3;
            }
            else if (checkPixelKey('storyCheckArea4')) {
                $currentArea = 4;
            }
            else if (checkPixelKey('storyCheckArea5')) {
                $currentArea = 5;
            }
            else if (checkPixelKey('storyCheckArea6')) {
                $currentArea = 6;
            }
            else if (checkPixelKey('storyCheckArea7')) {
                $currentArea = 7;
            }
            else if (checkPixelKey('storyCheckArea8')) {
                $currentArea = 8;
            }
            else if (checkPixelKey('storyCheckArea9')) {
                $currentArea = 9;
            }
            else if (checkPixelKey('storyCheckArea10')) {
                $currentArea = 10;
            }
            echo 'currentArea = ' . $currentArea . PHP_EOL;

            if ($area == 1 && $currentArea != $area) {
                switch ($currentArea) {
                    case 2: leftMap(1); break;
                    case 3: leftMap(2); break;
                    case 4: leftMap(3); break;
                    case 5: leftMap(4); break;
                    case 6: leftMap(5); break;
                }
            }
            else if ($area == 2 && $currentArea != $area) {
                switch ($currentArea) {
                    case 1: rightMap(1); break;
                    case 3: leftMap(1); break;
                    case 4: leftMap(2); break;
                    case 5: leftMap(3); break;
                    case 6: leftMap(4); break;
                }
            }
            else if ($area == 3 && $currentArea != $area) {
                switch ($currentArea) {
                    case 1: rightMap(2); break;
                    case 2: rightMap(1); break;
                    case 4: leftMap(1); break;
                    case 5: leftMap(2); break;
                    case 6: leftMap(3); break;
                }
            }
            else if ($area == 4 && $currentArea != $area) {
                switch ($currentArea) {
                    case 1: rightMap(3); break;
                    case 2: rightMap(2); break;
                    case 3: rightMap(1); break;
                    case 5: leftMap(1); break;
                    case 6: leftMap(2); break;
                }
            }
            else if ($area == 5 && $currentArea != $area) {
                switch ($currentArea) {
                    case 1: rightMap(4); break;
                    case 2: rightMap(3); break;
                    case 3: rightMap(2); break;
                    case 4: rightMap(1); break;
                    case 6: leftMap(1); break;
                }
            }
            else if ($area == 6 && $currentArea != $area) {
                switch ($currentArea) {
                    case 1: rightMap(5); break;
                    case 2: rightMap(4); break;
                    case 3: rightMap(3); break;
                    case 4: rightMap(2); break;
                    case 5: rightMap(1); break;
                }
            }
            else if ($area == 7 && $currentArea != $area) {
                switch ($currentArea) {
                    case 8: leftMap(1); break;
                    case 9: leftMap(2); break;
                    case 10: leftMap(3); break;
                }
            }
            else if ($area == 8 && $currentArea != $area) {
                switch ($currentArea) {
                    case 7: rightMap(1); break;
                    case 9: leftMap(1); break;
                    case 10: leftMap(2); break;
                }
            }
            else if ($area == 9 && $currentArea != $area) {
                switch ($currentArea) {
                    case 7: rightMap(2); break;
                    case 8: rightMap(1); break;
                    case 10: leftMap(1); break;
                }
            }
            else if ($area == 10 && $currentArea != $area) {
                switch ($currentArea) {
                    case 7: rightMap(3); break;
                    case 8: rightMap(2); break;
                    case 9: rightMap(1); break;
                }
            }
            else {
                clickKey("area$area-$stage");
            }
        }
        checkSpecifyKey('characterPopupX');
        checkKey('storyWithFriend');
        $startStoryResult = checkKey('storyStartFight');
        if ($startStoryResult) {
            $supportCount = 0;
        }
        checkKey('storySelectCard');
        checkKey('storySelectCardConfirm');
        $startStoryResult2 = checkKey('nowRetry');
        checkKey('storyDeadExit');

        // 시련의탑에서 오는 경우 전투 시작 X
        checkKey('storyOrdealFightX');
        checkSpecifyKey('storyOrdealX');
        checkKey('storyChallengeX');
        if (checkKey('storyStaminaCancel') && $isChallenge > 0) {
            $mode = 'fight';
        }
    }
    else if ($mode == 'fight') {
        checkKey('fightStoryStartX');
        checkKey('fightGoLobby');
        if (checkKey('fightChallenge')) {
            sleep(3);
            clickKey('fightFight');
            sleep(3);
            clickKey('fightParty1');
            sleep(2);
            clickKey('fightParty2');
            sleep(2);
            clickKey('fightParty3');
        }
        $startFightResult = checkKey('fightReady');
        checkKey('fightStart');
        checkKey('fightContinue');
        checkKey('fightContinue2');
        checkKey('fightExit');

        if (checkKey('fightStaminaCancel1') || checkKey('fightStaminaCancel2') || checkKey('fightStaminaCancel3')) {
            $mode = 'tower';
        }
    }
    else if ($mode == 'tower') {
        checkKey('towerFightEndConfirm');
        if (checkKey('towerChallenge') || checkSpecifyKey('towerFightX')) {
            sleep(2);
            clickKey('towerTower');
            sleep(3);
            clickKey('towerParty1');
            sleep(3);
            clickKey('towerParty2');
            sleep(3);
            clickKey('towerParty3');
        }

        $startTowerResult = checkKey('towerReady');
        checkKey('towerNoSupport');
        checkKey('towerStart');
        checkKey('towerExit');
        checkKey('towerConfirm');
        checkKey('towerCancelBoast');
        checkKey('towerPackageClose');

        if (checkKey('towerStaminaCancel')) {
            $mode = 'ordeal';
            sleep(1);
            clickKey('ordealTowerFightX');
        }
    }
    else if ($mode == 'ordeal') {
        if (checkKey('ordealChallenge') || checkSpecifyKey('ordealTowerX')) {
            sleep(2);
            clickKey('ordealOrdeal');
        }
        // 스테이지가 맨위에 셋팅된 경우 아래로 드래그.
        if (checkPixelKey('ordealMain')) {
            dragKey('ordealStageDrag');
            sleep(1);
            dragKey('ordealStageDrag');
            sleep(1);
            dragKey('ordealStageDrag');
            sleep(1);
            dragKey('ordealStageDrag');
            sleep(1);
            dragKey('ordealStageDrag');
            sleep(1);
            clickKey('ordealStage50');
        }
        checkKey('ordealReady');
        checkKey('ordealNoSupport');
        checkKey('ordealStart');
        checkKey('ordealExit');
        checkKey('ordealFailConfirm');
        checkKey('ordealRewardConfirm');
        if (checkKey('ordealStaminaCancel')) {
            $mode = 'story';
        }
    }

    if ($startStoryResult == false && $startStoryResult2 == false && $startFightResult == false && $startTowerResult == false) {
        $stopCount++;
        if ($stopCount == 300) {
            echo 'stop!!' . PHP_EOL;
            exec("adb -s {$deviceName} shell am force-stop com.ftt.hero_gl_4kakao");
            sleep(10);
            $mode = 'story';
            $isFirst = true;
        }
    } else {
        $startStoryResult = false;
        $startStoryResult2 = false;
        $startFightResult = false;
        $startTowerResult = false;
        $startOrdealResult = false;
        $stopCount = 0;
    }
    echo 'supportCount = ' . $supportCount . PHP_EOL;
    echo 'stopCount = ' . $stopCount . PHP_EOL;
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

function leftMap($count = 0) {
    global $data;
    for ($i = 0; $i < $count; $i++) {
        clickEvent($data['leftMap'][0], $data['leftMap'][1]);
        sleep(2);
    }
}

function rightMap($count = 0) {
    global $data;
    for ($i = 0; $i < $count; $i++) {
        clickEvent($data['rightMap'][0], $data['rightMap'][1]);
        sleep(2);
    }
}

function checkKey($key) {
    global $data;
    return checkClick($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
}
function checkSpecifyKey($key) {
    global $data;
    return checkSpecifyClick($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3], $data[$key][4], $data[$key][5]);
}
function clickKey($key) {
    global $data;
    clickEvent($data[$key][0], $data[$key][1]);
}
function checkPixelKey($key) {
    global $data;
    return checkPixel($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
}
function dragKey($key) {
    global $data, $device;
    if ($device == 'genymotion') {
        drag($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
    } else if ($device == 'g2') {
        drag_g2($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
    }

}

function checkClick($x, $y, $color, $ment = '')
{
    global $img;
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($x, $y);
        echo $ment . ' click!' . PHP_EOL;
    }
    return $result;
}
function checkSpecifyClick($x, $y, $color, $clickX, $clickY, $ment)
{
    global $img;
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($clickX, $clickY);
        echo $ment . ' specify click!' . PHP_EOL;
    }
    return $result;
}
function checkPixel($x, $y, $color, $ment)
{
    global $img;
    $rgb = imagecolorat($img, $x, $y);
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    $a = ($rgb >> 24) & 0x7F;
    var_dump("$ment = $rgb, $r,$g,$b,$a");
    if ($rgb == $color)
    {
        return true;
    }
    return false;
}
function clickEvent($x, $y)
{
    global $deviceName, $device;
    $exec = "adb -s {$deviceName} shell \"";
    if ($device == 'genymotion') {
        $exec .= sendEvent('event7', 0x1, 0x14a, 0x1);
        $exec .= sendEvent('event7', 0x3, 0x3a, 0x1);
        $exec .= sendEvent('event7', 0x3, 0x35, $x);
        $exec .= sendEvent('event7', 0x3, 0x36, $y);
        $exec .= sendEvent('event7', 0x0, 0x2, 0x0);
        $exec .= sendEvent('event7', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event7', 0x1, 0x14a, 0x0);
        $exec .= sendEvent('event7', 0x3, 0x3a, 0x0);
        $exec .= sendEvent('event7', 0x3, 0x35, $x);
        $exec .= sendEvent('event7', 0x3, 0x36, $y);
        $exec .= sendEvent('event7', 0x0, 0x2, 0x0);
        $exec .= sendEvent('event7', 0x0, 0x0, 0x0);
    } elseif ($device == 'g2') {
        $exec .= sendEvent('event0', 0x3, 0x39, 0xa1);
        $exec .= sendEvent('event0', 0x3, 0x35, $x);
        $exec .= sendEvent('event0', 0x3, 0x36, $y);
        $exec .= sendEvent('event0', 0x3, 0x30, 0xd);
        $exec .= sendEvent('event0', 0x3, 0x31, 0x9);
        $exec .= sendEvent('event0', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event0', 0x3, 0x39, 0xffffffff);
        $exec .= sendEvent('event0', 0x0, 0x0, 0x0);
    } elseif ($device == 'galaxy3') {
        $exec .= sendEvent('event1', 0x3, 0x39, 0xa1);
        $exec .= sendEvent('event1', 0x3, 0x35, $x);
        $exec .= sendEvent('event1', 0x3, 0x36, $y);
        $exec .= sendEvent('event1', 0x3, 0x30, 0xd);
        $exec .= sendEvent('event1', 0x3, 0x31, 0x9);
        $exec .= sendEvent('event1', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event1', 0x3, 0x39, 0xffffffff);
        $exec .= sendEvent('event1', 0x0, 0x0, 0x0);
    }
    $exec .= "\"";
    exec($exec);
}
function drag($startX, $startY, $endX, $endY, $event = 'event7')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= "\"";
    exec($exec);
    $splitValue = 30;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x2, 0x0);
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}
function drag_g2($startX, $startY, $endX, $endY, $event = 'event0')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x39, 0x155);
    $exec .= "\"";
    exec($exec);
    $splitValue = 30;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}
function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
